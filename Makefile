all:
	${CC} mmenu.c -o mmenu -lXm -lXt -lcurl ${LDFLAGS} -std=c99 ${CFLAGS} -Wall \
		-Wextra
	scdoc < mmenu.1.scd > mmenu.1

install:
	mkdir -p /usr/local/bin /usr/local/man
	cp mmenu /usr/local/bin/mmenu
	cp mmenu.1 /usr/local/man/man1/mmenu.1

uninstall:
	rm /usr/local/bin/mmenu
	rm /usr/local/man/man1/mmenu.1
